﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PasswordManager
{
    public class Program
    {
        public static List<string> passwords = new List<string>();
        public static List<string> commands = new List<string>()
        {
            "добавить",
            "обновить",
            "удалить",
            "найти",
            "найти по левенштейну",
            "показать все"
        };

        public static void Main(string[] args)
        {
            Console.WriteLine("Password Manager v1.0\n");
            Console.WriteLine("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
            StartProgramm();
        }

        public static void StartProgramm()
        {
            Console.Write("\nДоступные команды: ");
            foreach (var command in commands)
            {
                if (command == "показать все")
                {
                    Console.WriteLine($"{command}");
                }
                else
                {
                    Console.Write($"{command}, ");
                }
            }
            WaitForInput();
        }

        public static void WaitForInput()
        {
            Console.Write("\nВведите команду: ");
            var input = Console.ReadLine();
            CheckCommand(input);
        }

        public static void CheckCommand(string input)
        {
            switch (input.ToLower())
            {
                case "добавить":
                    Console.Write("\nВведите пароль: ");
                    var password = Console.ReadLine();
                    try
                    {
                        AddPasswordController(password);
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                        StartProgramm();
                    }
                    break;
                case "обновить":
                    Console.Write("\nВведите порядковый номер и новый пароль(1,test): ");
                    var newPassword = Console.ReadLine();
                    try
                    {
                        UpdatePasswordController(newPassword);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        StartProgramm();
                    }
                    break;
                case "удалить":
                    Console.Write("\nВведите порядковый номер: ");
                    var index = Console.ReadLine();
                    try
                    {
                        DeletePasswordController(index);
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine(ex);
                        StartProgramm();
                    }
                    break;
                case "найти":
                    Console.Write("\nВведите строку, которую в содержит в себе пароль: ");
                    var text = Console.ReadLine();
                    try
                    {
                        SearchPasswordController(text);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        StartProgramm();
                    }
                    break;
                case ("найти по левенштейну"):
                    Console.Write("\nВведите строку, по которой будет идти поиск: ");
                    var lev = Console.ReadLine();
                    try
                    {
                        SearchByLevController(lev);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        StartProgramm();
                    }
                    break;
                case "показать все":
                    try
                    {
                        ShowAllController();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex);
                        StartProgramm();
                    }
                    break;
                default:
                    Console.WriteLine("Такой команды не существует!");
                    WaitForInput();
                    break;

            }
        }

        public static void SearchByLevController(string lev)
        {
            List<string> levPasswords = SearchByLev(lev);
            if (levPasswords.Count > 0)
            {
                foreach(var pass in levPasswords)
                {
                    Console.Write($"{pass} ");
                    WaitForInput();
                }
            }
            else
            {
                throw new Exception("Нет ни единого пароля!");
            }
        }

        public static List<string> SearchByLev(string lev)
        {
            List<string> levPasswords = passwords.ToList()
                                                 .Where(p => Levenshtein.GetLev(lev, p.ToLower()))
                                                 .ToList();
            return levPasswords;
        }

        public static void ShowAllController()
        {
            if(passwords.Count != 0)
            {
                foreach(var pass in passwords)
                {
                    Console.WriteLine($"{pass} ");
                }
                WaitForInput();
            }
            else
            {
                throw new Exception("Нет ни единого пароля!");
            }
        }

        public static void SearchPasswordController(string text)
        {
            if(passwords.Find(p => p.Contains(text))!= null)
            {
                Console.WriteLine(SearchPassword(text));
                WaitForInput();
            }
            else
            {
                throw new Exception("Такого пароля нет!");
            }
        }

        public static string SearchPassword(string text)
        {
            return passwords.Find(p => p.Contains(text));

        }

        public static void DeletePasswordController(string index)
        {
            if (DeletePassword(index))
            {
                Console.WriteLine("Пароль успешно удалён!");
                WaitForInput();
            }
            else
            {
                throw new Exception("Пароля с таким индексом не существует!");
            }
        }

        public static bool DeletePassword(string index)
        {
            if (passwords.ElementAt(Convert.ToInt32(index)) != null)
            {
                passwords.RemoveAt(Convert.ToInt32(index));
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void UpdatePasswordController(string newPassword)
        {
            var indexPassword = newPassword.Split(',');
            if (passwords.ElementAt(Convert.ToInt32(indexPassword[0])) != null)
            {
                if (CheckPassword(indexPassword[1]))
                {
                    passwords[Convert.ToInt32(indexPassword[0])] = indexPassword[1];
                    Console.WriteLine("Пароль успешно изменён!");
                    WaitForInput();
                }
                else
                {
                    throw new Exception("Пароль не соответсвует требованиям");
                }
            }
            else
            {
                throw new Exception("Пароля с таким индексом не существует!");
            }
        }

        public static void AddPasswordController(string password)
        {
            if (AddPassword(password))
            {
                Console.WriteLine("Пароль успешно добавлен!");
                WaitForInput();
            }
            else
            {
                throw new Exception("Пароль не соответсвует требованиям");
            }
        }

        public static bool AddPassword(string password)
        {
            if (CheckPassword(password))
            {
                passwords.Add(password);
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool CheckPassword(string password)
        {
            return Regex.IsMatch(password, "^.*(?=.{8,})(?=.*[a-zA-Z])(?=.*\\d)(?=.*[!#$%&? \"]).*$");
        }
    }
}
