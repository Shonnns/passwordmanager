﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using PasswordManager;
using System.Collections.Generic;
using System.Linq;

namespace PasswordManagerTestss
{
    [TestClass]
    public class ProgramTests
    {
        [TestMethod]
        public void AddPassword_ValidPassword_PasswordAdded()
        {
            // Arrange
            string password = "!qwerER1234";
            
            // Act
            bool actual = Program.AddPassword(password);

            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void AddPassword_ShortPassword_PasswordNotAdded()
        {
            // Arrange
            string password = "1#wW";

            // Act
            bool actual = Program.AddPassword(password);

            Assert.IsFalse(actual);
        }


        [TestMethod]
        public void AddPassword_onlyLetters_PasswordNotAdded()
        {
            // Arrange
            string password = "qwertyuiiop";

            // Act
            bool actual = Program.AddPassword(password);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void DeletePassword_ValidIndex_PasswordRemoved()
        {
            // Arrange
            string password = "!qwerER1234";
            string index = "0";

            // Act
            Program.AddPassword(password);
            bool actual = Program.DeletePassword(index);

            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void DeletePassword_NotValidIndex_PasswordRemoved()
        {
            // Arrange
            string password = "!qwerER1234";
            string index = "1";

            // Act
            Program.AddPassword(password);
            bool actual = Program.DeletePassword(index);

            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void SearchPassword_FullValid_PasswordSearched()
        {
            // Arrange
            string password = "!qwerER1234";
            string search_password = "!qwerER1234";

            // Act
            Program.AddPassword(password);
            string actual = Program.SearchPassword(search_password);

            Assert.AreEqual(password, actual);
        }

        [TestMethod]
        public void SearchPassword_NotFullValid_PasswordSearched()
        {
            // Arrange
            string password = "!qwerER1234";
            string search_password = "!qwer";

            // Act
            Program.AddPassword(password);
            string actual = Program.SearchPassword(search_password);

            Assert.AreEqual(password, actual);
        }

        [TestMethod]
        public void SearchByLev_FullValid_PasswordSearched()
        {
            // Arrange
            string password = "!qwerER1234";
            string search_password = "!qwerER1234";

            // Act
            Program.AddPassword(password);
            List<string> actual = Program.SearchByLev(search_password);

            Assert.AreEqual(password, actual.First());
        }

        [TestMethod]
        public void SearchByLev_3Erorrs_PasswordSearched()
        {
            // Arrange
            string password = "!qwerER1234";
            string search_password = "!qwerER1122";

            // Act
            Program.AddPassword(password);
            List<string> actual = Program.SearchByLev(search_password);

            Assert.AreEqual(password, actual.First());
        }
    }
}
